
const fname = document.querySelector("#fname");
const lname = document.querySelector("#lname");
const fullName = document.querySelector("#fullName");


//reset fields
function myReset() {
	fname.value = '';
	lname.value = '';	
	fullName.innerHTML = '';
}

//fname event listener
fname.addEventListener('keyup', printFullName);
//lname event listener
lname.addEventListener('keyup',printFullName);


function printFullName(){
	fullName.innerHTML = `${fname.value}  ${lname.value}`;
}


// document.querySelectorAll('input[type=text]').forEach(item => {
//   item.addEventListener('keyup', event => {
//     fullName.innerHTML = `${fname.value}  ${lname.value}`;
//   })
// })



