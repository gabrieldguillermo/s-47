// console.log('Hello World');

console.log(document.querySelector("#fname"))


// DOM Manipulation

const fname = document.querySelector("#fname");
const lname = document.querySelector("#lname");
const fullName = document.querySelector("#fullName");

console.log(fname, fullName);


/*
	Event:
		click, hover, keypress, keyup


	Event Listeners:
		Allows us to let our users interact with our page. Each click or hover is an event which can trigger a function or task.


	Syntax:
		selectedElement.addEventListener('event',function);	
*/


// const fnameLabel = document.querySelector("#fname-label");

// fnameLabel.addEventListener('click', (e) => {
//     alert('You click first name')
// });



//reset fields

function myReset() {
	fname.value = '';
	lname.value = '';	
	fullName.innerHTML = '';
}


document.querySelectorAll('input[type=text]').forEach(item => {
  item.addEventListener('keyup', event => {
    fullName.innerHTML = `${fname.value}  ${lname.value}`;
  })
})



